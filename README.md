# Resolve Value Task
### Getting Started:
- Clone this repository
- Change directory by running `cd resolve-value-task` command
- Run `npm install` to install dependencies
- Run test: `npm run test`