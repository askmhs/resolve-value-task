import fs from "fs";
import assert from "assert";
import yaml from "js-yaml";
import resolveConfig from "./../../src/resolver";

describe("Resolve config test", () => {

    const YAMLServicePath = __dirname + "/../../service.yml";
    const JSONServicePath = __dirname + "/../../service.json";
    const DotEnvPath = __dirname + "/../../.env";
    const JSONEnvPath = __dirname + "/../../env.json";

    /**
     * Validate service from YAML
     *
     * @param params
     * @param config
     * @param envVariables
     */
    const validate = (params, config, envVariables) => {
        Object.keys(params).map(key => {
            if (typeof params[key] === "object") {
                return validate(params[key], config[key], envVariables);
            } else {
                assert.deepStrictEqual(params[key], "" + envVariables[config[key].replace(/[^\w\s]/gi, '')]);
                assert.deepStrictEqual(config.hasOwnProperty(key), true);
            }
        });
    };

    describe("Using YAML config and .env file", () => {
        let result = {}, parsedConfig = {};

        before(done => {
            require("dotenv").config(DotEnvPath);
            result = resolveConfig(YAMLServicePath, DotEnvPath);
            parsedConfig = yaml.safeLoad(fs.readFileSync(YAMLServicePath));
            done();
        });

        it("Should return an object", done => {
            assert.deepStrictEqual(typeof result, "object");
            done();
        });

        it("Should return a valid object that contains values from environment variables", done => {
            validate(result, parsedConfig, process.env);
            done();
        });
    });

    describe("Using JSON config and .env file", () => {
        let result = {};
        const config = require(JSONServicePath);

        before(done => {
            require("dotenv").config(DotEnvPath);
            result = resolveConfig(config, DotEnvPath);
            done();
        });

        it("Should return an object", done => {
            assert.deepStrictEqual(typeof result, "object");
            done();
        });

        it("Should return a valid object that contains values from environment variables", done => {
            assert.deepStrictEqual(config, result);
            done();
        });
    });

    describe("Using YAML config and JSON environment", () => {
        let result = {}, parsedConfig = {};

        before(done => {
            result = resolveConfig(YAMLServicePath, JSONEnvPath);
            parsedConfig = yaml.safeLoad(fs.readFileSync(YAMLServicePath));
            done();
        });

        it("Should return an object", done => {
            assert.deepStrictEqual(typeof result, "object");
            done();
        });

        it("Should return a valid object that contains values from environment variables", done => {
            validate(result, parsedConfig, require(JSONEnvPath));
            done();
        });
    });

    describe("Using JSON config and JSON environment", () => {
        let result = {};
        const config = require(JSONServicePath);

        before(done => {
            result = resolveConfig(require(JSONServicePath), require(JSONEnvPath));
            done();
        });

        it("Should return an object from", done => {
            assert.deepStrictEqual(typeof result, "object");
            done();
        });

        it("Should return a valid object that contains values from environment variables", done => {
            assert.deepStrictEqual(config, result);
            done();
        });
    });
});