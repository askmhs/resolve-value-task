const requireDir = require("require-dir");

/**
 * Load all unit test files
 */
requireDir("./unit");