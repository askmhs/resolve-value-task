/**
* @Author: askmhs
* @Date:   2019-08-14 22:26:55
* @Last Modified by:   Muhammad Harits Syaifulloh
* @Last Modified time: 2019-08-15 07:35:07
*/
import fs from "fs";
import yaml from "js-yaml";

/**
 * Process config, set value for parsed configuration from yaml from .env variables
 *
 * @param configService
 * @param envVariables
 * @returns {*}
 */
function processConfig(configService, envVariables) {
	Object.keys(configService).map(key => {
		if (typeof configService[key] === "object") {
			return processConfig(configService[key], envVariables);
		} else {
			configService[key] = envVariables[configService[key].replace(/[^\w\s]/gi, '')];
		}
	});

	return configService;
}

/**
 * Resolve config, parse configuration from service.yaml file
 *
 * @returns {*}
 * @param service
 * @param env
 */
function resolveConfig(service, env) {
	service = (typeof service === "object") ? service : yaml.safeLoad(fs.readFileSync(service));

	/**
	 * Load environment variables from given .env path
	 */
	require("dotenv").config({
		path: env
	});

	env = (typeof env === "object") ? env : process.env;

	return processConfig(service, env);
}

module.exports = resolveConfig;